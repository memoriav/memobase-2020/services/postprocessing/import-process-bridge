/*
 * Copyright (C) 2022 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.apache.jena.graph.Graph
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.shacl.ShaclValidator
import org.apache.jena.shacl.Shapes
import java.io.StringWriter

class ShaclValidator(
    private val validationFile: String,
) {
    private val validator = ShaclValidator.get()

    fun validate(data: Model): Pair<Boolean, String> {
        val shapesGraph: Graph = RDFDataMgr.loadGraph(validationFile)
        val dataGraph: Graph = data.graph

        val shapes: Shapes = Shapes.parse(shapesGraph)

        val report = validator.validate(shapes, dataGraph)

        StringWriter().use {
            RDFDataMgr.write(it, report.graph, Lang.TTL)
            return Pair(report.conforms(), it.toString())
        }
    }
}
