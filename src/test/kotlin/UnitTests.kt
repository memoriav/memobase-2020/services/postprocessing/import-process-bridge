/*
 * Copyright (C) 2022 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import ch.memobase.model.MemobaseModel
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File
import java.io.FileInputStream

internal class UnitTests {
    private val basePath = "src/test/resources"
    private val log = LogManager.getLogger()

    @Test
    fun testInternalMapping() {
        val model =
            MemobaseModel(
                false,
                "https://media.memobase.k8s.unibas.ch/memo/",
                "$basePath/frame-record.json",
            )
        val input = File("$basePath/input-value.nt").inputStream()
        RDFDataMgr.read(model, input, Lang.NT)
        val output = model.toString()
        assert(output.contains("\"isPublished\":true"))
    }

    @Test
    fun `test replace locator with local locator`() {
        val model =
            MemobaseModel(
                true,
                "https://media.memobase.k8s.unibas.ch/memo/",
                "$basePath/frame-record.json",
                RDFFormat.JSONLD10_FRAME_PRETTY,
            )
        RDFDataMgr.read(model, FileInputStream("$basePath/replace-locator-input-local.ttl"), Lang.TTL)
        model.replaceLocator()
        val validator = ShaclValidator("$basePath/replace-locator-input-local-validation.ttl")
        val validationOutput = validator.validate(model)
        log.error(validationOutput.second)
        assertThat(validationOutput.first)
            .isTrue
    }

    @Test
    fun `test replace locator with remote locator`() {
        val model =
            MemobaseModel(
                true,
                "https://media.memobase.k8s.unibas.ch/memo/",
                "$basePath/frame-record.json",
                RDFFormat.JSONLD10_FRAME_PRETTY,
            )

        RDFDataMgr.read(model, FileInputStream("$basePath/replace-locator-input-remote.ttl"), Lang.TTL)

        model.replaceLocator()

        val validator = ShaclValidator("$basePath/replace-locator-input-remote-validation.ttl")
        val validationOutput = validator.validate(model)
        log.error(validationOutput.second)
        assertThat(validationOutput.first)
            .isTrue
    }
}
