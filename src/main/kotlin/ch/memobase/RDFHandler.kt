/*
 * Copyright (C) 2022 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.exceptions.InvalidInputException
import ch.memobase.model.MemobaseModel
import ch.memobase.model.ProcessResult
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.HeaderMetadata
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RiotException
import org.apache.logging.log4j.LogManager
import java.nio.charset.StandardCharsets

class RDFHandler(
    private val framed: Boolean,
    private val mediaServerUrl: String,
    private val frameRecordPath: String,
) {
    private val log = LogManager.getLogger(this::class.java)

    fun parseRDFInput(input: Pair<String, HeaderMetadata>): ProcessResult<Model> {
        val model = MemobaseModel(framed, mediaServerUrl, frameRecordPath)
        try {
            RDFDataMgr.read(
                model, input.first.byteInputStream(StandardCharsets.UTF_8), if (framed) Lang.JSONLD else Lang.NT
            )
        } catch (ex: RiotException) {
            log.error("Parsing error: ${ex.message}")
            log.debug(ex.stackTrace)
            return ProcessResult(model, ReportStatus.fatal, "Parsing of Ntriples failed!")
        }
        return ProcessResult(model, ReportStatus.success, "Parsing successful")
    }

    fun writeJsonLd(input: ProcessResult<Model>): ProcessResult<String> {
        return try {
            val data = input.data.toString()
            if (input.status != ReportStatus.success) {
                ProcessResult("", input.status, input.message)
            } else {
                ProcessResult(data, input.status, "Transformation successful")
            }
        } catch (ex: InvalidInputException) {
            ProcessResult("", ReportStatus.fatal, "Invalid Input Exception: ${ex.localizedMessage}")
        }
    }
}