/*
 * Copyright (C) 2022 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsConfig
import org.apache.logging.log4j.LogManager
import kotlin.system.exitProcess

class Service(file: String = "app.yml") {
    private val log = LogManager.getLogger(this::class.java)

    companion object {
        const val OUTPUT_FRAMED_JSONLD_PROP_NAME = "outputFramedJsonLD"
        const val MEDIA_SERVER_URL_PROP_NAME = "mediaServerUrl"
        const val REPORTING_STEP_NAME_PROP_NAME = "reportingStepName"
        const val APP_VERSION = "appVersion"
        const val FRAME_RECORD_PATH = "frameRecordPath"
    }

    private val settings =
        SettingsLoader(
            listOf(
                OUTPUT_FRAMED_JSONLD_PROP_NAME,
                MEDIA_SERVER_URL_PROP_NAME,
                REPORTING_STEP_NAME_PROP_NAME,
                FRAME_RECORD_PATH,
                APP_VERSION,
            ),
            fileName = file,
            useStreamsConfig = true,
        )


    init {
        settings.kafkaStreamsSettings[StreamsConfig.producerPrefix("max.request.size")] = "2097152"
    }

    private val topology = KafkaTopology(settings).build()
    private val stream = KafkaStreams(topology, settings.kafkaStreamsSettings)

    fun run() {
        stream.use {
            it.start()
            while (stream.state().isRunningOrRebalancing) {
                Thread.sleep(10_000L)
            }

            if (stream.state().isShuttingDown) {
                log.info("Shutting application down now...")
                exitProcess(0)
            } else {
                throw Exception("Should not happen.")
            }
        }
    }
}
