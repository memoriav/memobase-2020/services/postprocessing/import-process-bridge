/*
 * Copyright (C) 2022 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.model.ProcessResult
import ch.memobase.reporting.Report
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.LogManager

class KafkaTopology(private val settings: SettingsLoader) {
    private val log = LogManager.getLogger(this::class.java)
    private val recordPipelineStep = settings.appSettings.getProperty(Service.REPORTING_STEP_NAME_PROP_NAME)
    private val rdfHandler =
        RDFHandler(
            settings.appSettings.getProperty(Service.OUTPUT_FRAMED_JSONLD_PROP_NAME)!!.toBoolean(),
            settings.appSettings.getProperty(Service.MEDIA_SERVER_URL_PROP_NAME),
            settings.appSettings.getProperty(Service.FRAME_RECORD_PATH)
        )

    fun build(): Topology {
        val builder = StreamsBuilder()

        val stream = builder.stream<String, String>(settings.inputTopic)
            .filter { key, value ->
                if (value == null) {
                    log.warn("Ignored message with key '$key' as its value is null.")
                    false
                } else {
                    true
                }
            }
        log.info("Enabled record pipeline with name $recordPipelineStep!")
        val processOutcome = stream
            .processValues(HeaderExtractionSupplier<String>())
            .mapValues { value -> rdfHandler.parseRDFInput(value) }
            .mapValues { value -> rdfHandler.writeJsonLd(value) }

        processOutcome
            .mapValues { key, value -> writeReport(key, value, recordPipelineStep) }
            .to(settings.processReportTopic)

        processOutcome
            .mapValues { value -> value.data }
            .to(settings.outputTopic)

        return builder.build()
    }

    private fun writeReport(id: String, input: ProcessResult<String>, step: String): String {
        return Report(id, input.status, input.message, step, settings.appSettings.getProperty(Service.APP_VERSION)).toJson()
    }
}
