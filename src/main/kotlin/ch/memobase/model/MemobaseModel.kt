/*
 * Copyright (C) 2022 - present  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ch.memobase.model

import ch.memobase.exceptions.InvalidInputException
import ch.memobase.rdf.EBUCORE
import ch.memobase.rdf.MB
import ch.memobase.rdf.NS
import ch.memobase.rdf.RICO
import com.beust.klaxon.Klaxon
import com.github.jsonldjava.core.JsonLdOptions
import org.apache.jena.graph.Factory
import org.apache.jena.rdf.model.Statement
import org.apache.jena.rdf.model.impl.ModelCom
import org.apache.jena.riot.JsonLDWriteContext
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.jena.riot.writer.JsonLD10Writer
import org.apache.jena.shared.JenaException
import org.apache.jena.sparql.core.DatasetGraphFactory
import org.apache.logging.log4j.LogManager
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.StringWriter
import java.nio.charset.Charset

class MemobaseModel(
    private val framed: Boolean,
    private val mediaServerUrl: String,
    private val framePath: String,
    private val format: RDFFormat = if (framed) RDFFormat.JSONLD10_FRAME_FLAT else RDFFormat.JSONLD10_COMPACT_FLAT,
) : ModelCom(Factory.createGraphMem()) {
    private val log = LogManager.getLogger(this::class.java)
    private val context = JsonLDWriteContext()
    private val options = JsonLdOptions()
    private val klaxon = Klaxon()

    fun replaceLocator() {
        val digitalObjects = this.listResourcesWithProperty(RICO.type, RICO.Types.Instantiation.digitalObject).toList()
        val digitalObject =
            if (digitalObjects.size == 1) {
                digitalObjects.first()
            } else {
                null
            }
        if (digitalObject != null) {
            if (digitalObject.hasProperty(EBUCORE.locator)) {
                val locator = try {
                    digitalObject.getProperty(EBUCORE.locator).`object`.asLiteral().string
                } catch (ex: JenaException) {
                    log.error("Object on ebucore:locator is not a literal.")
                    ""
                }
                digitalObject.removeAll(EBUCORE.locator)
                val statements = digitalObject.listProperties(RICO.hasOrHadIdentifier).toList()
                if (statements.isEmpty()) {
                    throw InvalidInputException("Digital object does not have any identifiers.")
                }
                val mainStatement = statements.firstOrNull { statement ->
                    if (statement.`object`.isResource) {
                        statement.`object`.asResource().hasProperty(RICO.type, RICO.Types.Identifier.main)
                    } else {
                        false
                    }
                }
                if (mainStatement != null) {
                    if (mainStatement.`object`.isResource) {
                        val resource = mainStatement.`object`.asResource()
                        val identifierStatement: Statement? = resource.getProperty(RICO.identifier)
                        if (identifierStatement != null) {
                            digitalObject.addProperty(
                                EBUCORE.locator, "${mediaServerUrl}${identifierStatement.string}"
                            )
                            digitalObject.addProperty(
                                MB.mediaLocation,
                                if (locator.startsWith("sftp:")) "local" else "remote"
                            )
                        } else {
                            throw InvalidInputException("Identifier in digital object ${digitalObject.uri} is not defined.")
                        }
                    } else {
                        throw InvalidInputException("Identifier in digital object ${digitalObject.uri} is not a object property as expected.")
                    }
                } else {
                    throw InvalidInputException("The digital object does not have a main identifier.")
                }
            } else {
                log.warn("The digital object does not have a locator to a media resource.")
            }
        } else {
            log.debug("This record does not contain a digital object.")
        }
    }

    private fun setupJsonLDContext() {
        context.setFrame(
            File(framePath).readText(Charset.defaultCharset())
        )
        options.omitGraph = true
        options.pruneBlankNodeIdentifiers = true
        options.compactArrays = true
        options.processingMode = JsonLdOptions.JSON_LD_1_0
        context.setOptions(options)
    }

    override fun toString(): String {
        if (framed) {
            replaceLocator()
            setupJsonLDContext()
            val output = ByteArrayOutputStream()
            val writer = JsonLD10Writer(RDFFormat.JSONLD10_FRAME_FLAT)
            writer.write(
                output, DatasetGraphFactory.create(this.graph), null, null, context
            )
            val json = klaxon.parseJsonObject(output.toString(Charset.defaultCharset()).reader())
            json.remove("@context")
            json["@context"] = "https://api.memobase.ch/context.json"
            return json.toJsonString()
        } else {
            val out = StringWriter()
            this.setNsPrefixes(NS.prefixMapping)
            RDFDataMgr.write(out, this, format)
            return out.toString().replace("internal:isPublished", "isPublished").trim()
        }
    }
}
